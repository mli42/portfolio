import { Source_Code_Pro } from 'next/font/google';

export const sourceCodePro = Source_Code_Pro({
  subsets: ['latin'],
  variable: '--font-source-code-pro',
});
