import classNames from 'classnames';
import { FC, MouseEventHandler, ReactNode } from 'react';

interface ButtonProps {
  className?: string;
  onClick?: MouseEventHandler;
  variant?: 'rounded';
  children?: ReactNode;
  title?: string;
}

const Button: FC<ButtonProps> = ({
  className,
  onClick,
  variant,
  children,
  title,
}) => {
  const variants = { rounded: classNames('rounded-full px-2 py-2') };

  return (
    <button
      className={classNames(
        'rounded border border-gray-600 bg-blue-200 px-5 py-2 hover:bg-blue-300 dark:border-gray-300 dark:bg-gray-800 dark:hover:bg-gray-700',
        variant !== undefined && variants[variant],
        className
      )}
      onClick={onClick}
      title={title}
    >
      {children}
    </button>
  );
};

export default Button;
