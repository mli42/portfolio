import ToggleColorModeButton from '@/components/Molecules/ToggleColorModeButton';
import ToggleLocaleButton from '@/components/Molecules/ToggleLocaleButton';
import React, { FC } from 'react';

const Navbar: FC = ({}) => {
  return (
    <div className="absolute right-5 top-5 flex flex-row items-center justify-center gap-x-2">
      <ToggleColorModeButton iconClass="h-6 w-6" />
      <ToggleLocaleButton iconClass="h-6 w-6" />
    </div>
  );
};

export default Navbar;
