import { Link } from '@/plugins/i18n';
import classNames from 'classnames';
import { getTranslations } from 'next-intl/server';
import SocialMedias from '../Molecules/SocialMedias';
import SvgWave from '../Svg/Wave';

interface FooterProps {
  className?: string;
}

const Footer: React.FC<FooterProps> = async ({ className }) => {
  const t = await getTranslations();
  const repoLink = 'https://gitlab.com/mli42/portfolio';

  return (
    <div
      className={classNames(
        'relative flex flex-col items-center justify-center gap-y-5',
        className
      )}
    >
      <SvgWave className="absolute bottom-full h-full text-white dark:text-dark" />
      <SocialMedias className="flex flex-row gap-x-6" iconClass="h-8 w-8" />

      {/* <!-- Footer text --> */}
      <p className="text-center text-xs text-gray-500">
        <span>{t('sourceCodeAt')} </span>
        <Link className="underline" href={repoLink} target="_blank">
          gitlab.com/mli42
        </Link>
        <br />
        <span>{t('freeBackground')}</span>
      </p>
    </div>
  );
};

export default Footer;
