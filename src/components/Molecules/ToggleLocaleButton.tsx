'use client';

import { Link, getNextLocale, usePathname } from '@/plugins/i18n';
import classNames from 'classnames';
import { useLocale } from 'next-intl';
import React, { FC } from 'react';
import Button from '../Atoms/Button';
import SvgTranslation from '../Svg/Translation';

interface Props {
  className?: string;
  iconClass?: string;
}

const ToggleLocaleButton: FC<Props> = ({ className, iconClass }) => {
  const locale = useLocale();
  const pathname = usePathname();
  const nextLocale = getNextLocale(locale);
  const localeSwitchLabel = locale === 'fr' ? 'EN' : 'FR';

  return (
    <Link className="rounded-full" href={pathname} locale={nextLocale}>
      <Button
        className={classNames(
          'flex flex-row items-center justify-center gap-x-1 px-3',
          className
        )}
        variant="rounded"
      >
        <SvgTranslation className={iconClass} />
        <span className="text-lg font-bold">{localeSwitchLabel}</span>
      </Button>
    </Link>
  );
};

export default ToggleLocaleButton;
