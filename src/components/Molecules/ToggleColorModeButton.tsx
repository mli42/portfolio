'use client';

import useIsMounted from '@/hooks/useIsMounted';
import classNames from 'classnames';
import { useTranslations } from 'next-intl';
import { useTheme } from 'next-themes';
import React, { FC } from 'react';
import Button from '../Atoms/Button';
import SvgMoon from '../Svg/Moon';
import SvgSun from '../Svg/Sun';

interface Props {
  className?: string;
  iconClass?: string;
}

const ToggleColorModeButton: FC<Props> = ({ className, iconClass }) => {
  const t = useTranslations();
  const isMounted = useIsMounted(); // Avoid Hydration Mismatch
  const { resolvedTheme: colorMode, setTheme } = useTheme();

  const colorModeTranslateKey =
    colorMode === 'dark' ? 'toggleLightMode' : 'toggleDarkMode';

  const toggleColorMode = () => {
    setTheme(colorMode === 'dark' ? 'light' : 'dark');
  };

  return (
    <Button
      onClick={isMounted ? toggleColorMode : undefined}
      title={isMounted ? t(colorModeTranslateKey) : undefined}
      variant="rounded"
      className={className}
    >
      <SvgSun className={classNames(iconClass, 'hidden dark:block')} />
      <SvgMoon className={classNames(iconClass, 'block dark:hidden')} />
    </Button>
  );
};

export default ToggleColorModeButton;
