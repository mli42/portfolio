import { Link } from '@/plugins/i18n';
import classNames from 'classnames';
import { FC } from 'react';
import SvgEmail from '../Svg/Email';
import SvgGithub from '../Svg/Github';
import SvgGitlab from '../Svg/Gitlab';
import SvgLinkedIn from '../Svg/LinkedIn';

const socialMedia = [
  {
    Icon: SvgLinkedIn,
    link: 'https://www.linkedin.com/in/marc-li-/',
    title: 'LinkedIn',
  },
  { Icon: SvgGithub, link: 'https://github.com/mli42', title: 'Github' },
  { Icon: SvgGitlab, link: 'https://gitlab.com/mli42', title: 'Gitlab' },
  { Icon: SvgEmail, link: 'mailto:marc.li.pro@proton.me', title: 'Email' },
];

interface SocialMediasProps {
  className?: string;
  iconClass?: string;
}

const SocialMedias: FC<SocialMediasProps> = ({ className, iconClass }) => {
  return (
    <div className={className}>
      {socialMedia.map(({ Icon, link, title }) => (
        <Link key={title} href={link} title={title} target="_blank">
          <Icon
            className={classNames('transition-all hover:scale-125', iconClass)}
          />
        </Link>
      ))}
    </div>
  );
};

export default SocialMedias;
