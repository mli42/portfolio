import Button from '@/components/Atoms/Button';
import Footer from '@/components/Organisms/Footer';
import { getTranslations, setRequestLocale } from 'next-intl/server';
import Image from 'next/image';

type PageProps = {
  params: Promise<Record<string, string>>;
};

export default async function Page({ params }: PageProps) {
  const locale = (await params).locale;
  setRequestLocale(locale);

  const t = await getTranslations();
  const resumeLink = locale === 'fr' ? '/MarcLi-CV.pdf' : '/MarcLi-Resume.pdf';

  return (
    <main className="flex h-screen w-full flex-col justify-between overflow-y-hidden">
      <div className="h-full bg-[linear-gradient(60deg,var(--tw-gradient-stops))] from-[rgb(84,58,183)] to-[rgb(0,172,193)] dark:from-[rgb(100,10,10)] dark:to-[rgb(0,70,70)]">
        <div className="flex h-5/6 flex-col items-center justify-center gap-y-4">
          <Image
            className="rounded-full border-2 border-white sm:h-48 sm:w-48"
            width="160"
            height="160"
            src="/avatar.webp"
            alt="Marc's profile picture"
            priority
          />
          <h1 className="text-4xl text-white sm:text-5xl">Marc Li</h1>
          <a
            className="mt-2"
            target="_blank"
            rel="noreferrer"
            href={resumeLink}
          >
            <Button>{t('downloadResume')}</Button>
          </a>
        </div>
      </div>

      <Footer className="h-1/4 animate-footer" />
    </main>
  );
}
