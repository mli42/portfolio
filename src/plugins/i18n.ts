import { routing } from '@/i18n/routing';
import { Locale } from '@/types/i18n';
import { createNavigation } from 'next-intl/navigation';

export const { Link, redirect, usePathname, useRouter } =
  createNavigation(routing);

export const getNextLocale = (locale: Locale) => {
  const locales = routing.locales;
  const len = locales.length;
  const currentLocalIndex = locales.indexOf(locale);

  return locales[(currentLocalIndex + 1) % len];
};
