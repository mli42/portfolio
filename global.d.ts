import { Locale } from '@/types/i18n';
import en from './locales/en.json';

type Messages = typeof en;

declare global {
  interface IntlMessages extends Messages {}
}

declare module 'next-intl' {
  function useLocale(): Locale;
}

declare module 'next-intl/server' {
  function getLocale(): Promise<Locale>;
}
