import type { Config } from 'tailwindcss';

const config: Config = {
  darkMode: 'selector',
  content: ['./src/**/*.{js,ts,jsx,tsx,mdx}'],
  corePlugins: {
    preflight: false,
  },
  theme: {
    extend: {
      colors: {
        dark: '#091A28',
      },
      fontFamily: {
        sourceCodePro: ['var(--font-source-code-pro)'],
      },
      keyframes: {
        footerEmerge: {
          '0%': { height: '0' },
          '75%': { height: '30%' },
        },
      },
      animation: {
        footer: 'footerEmerge 1.5s ease-in-out',
      },
    },
  },
  plugins: [],
};

export default config;
