NAME := portfolio_front
DOCKER := docker
DC := docker-compose

DOCKER_EXEC := $(DOCKER) exec -it $(NAME)

all: up

up:
	$(DC) up --build

sh:
	$(DOCKER_EXEC) sh

build:
	cp ../resume/CV_FR.pdf public/MarcLi-CV.pdf
	cp ../resume/CV_EN.pdf public/MarcLi-Resume.pdf
	$(DOCKER_EXEC) yarn build

deploy:
	$(DOCKER_EXEC) vercel login
	$(DOCKER_EXEC) vercel build --prod
	$(DOCKER_EXEC) vercel deploy --prebuilt --prod

re: fclean all

reload: down all

logs:
	$(DC) logs --follow --tail 1000

stop:
	$(DC) stop

down:
	$(DC) down

ps:
	$(DC) ps

fclean:
	$(DC) down --rmi all --volumes
	rm -rf ./.next
	rm -rf ./.vercel
	rm -rf ./node_modules
