FROM node:23-alpine3.20

WORKDIR /portfolio

# Setup husky
RUN apk add git && git config --global --add safe.directory /portfolio

RUN yarn global add vercel

CMD sleep infinity
